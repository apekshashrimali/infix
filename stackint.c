#include "stackint.h"
void init(stackint *s){
	s->top = -1;

}
int push(stackint *s,int data){
	if(isfull(s))
		return -1;
	else{
		s->top++;
		s->a[s->top] = data;
		return 1;
	}	
	
}
int pop(stackint *s){
	int c;
	if(isempty(s))
		return -1;
	else{
		c = s->a[s->top];
		s->top--;
		return c;
	}
}
int isempty(stackint *s){
	if(s->top == -1)
		return 1;
	return 0;
}
int isfull(stackint *s){
	if(s->top == MAX)
		return 1;
	return 0;
}
