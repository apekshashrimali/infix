#include<stdio.h>
#define MAX 100
typedef struct Stack{
	int a[MAX];
	int top;
}Stack;
void init(Stack *s);
int push(Stack *s,int data);
int pop(Stack *s);
int isempty(Stack *s);
int isfull(Stack *s);
