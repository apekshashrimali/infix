#include<stdio.h>
#include "stackint.h"
#include "stackchar.h"
#define ERR 50

int perform(int op1,int op2,char operand){
	switch(operand){
		case '*':
			return op1 * op2;
		case '/':
			return op1/op2;
		case '-':
			return op1 - op2;	
		case '+':
			return op1 + op2;
		default:
			return -1;
	}
}
int infix(char *exp){
	Stack si;
	stackchar sc;
	int digit;
	char operand;
	int op1,op2,result;
	int i = 0;
	initchar(&sc);
	init(&si);
	while(exp[i] != '\0'){

		switch(exp[i]){
			case '0':case '1':case '2':case '3':
			case '4':case '5':case '6':case '7':
			case '8':case '9':	
				digit = exp[i] - '0';
				if(isfull(&si)){
					printf("stack full");
					return ERR;
				}
				push(&si,digit);
				i++;
				break;
			case '+':case '-': case '*': case '/':
				operand = exp[i];
				if(isempty(&sc) || ((precedence(sc.ch[top])) < (precedence(operand))))
					pushchar(&sc,operand);
				else if(precedence(sc.ch[top]) > precedence(operand)){
					op1 = pop(&si);
					op2 = pop(&si);
					result = perform(op1,op2,operand);
					push(&si,result);			
				}
				if(isfull(&sc)){
					printf("op stack full");
					return ERR;			
				}
				i++;
				break;			
		}
	}

	if(isempty(&si))
		return -1;
	else
		return (pop(&si));
}
int main(void){
	char *exp;
	int r;
	printf("Enter the xprssion");
	scanf("%s",exp);
	if(r == -1)
		printf("error");
	else
		printf("Evaluated expression is %d",r);
	return 0;
}
